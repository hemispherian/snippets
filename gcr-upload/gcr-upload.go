package main

import (
    "bytes"
    "fmt"
    "io/ioutil"
    "log"
    "os"
    "time"
    "net/http"
    "strconv"
    "strings"
    "encoding/json"

    "google.golang.org/cloud/storage"
    "golang.org/x/oauth2/google"
    "io"
    "flag"
    "path/filepath"
)

var config map[string]string

func staticConfiguration() map[string]string {
    return map[string]string{
        "bucket": "", // The bucket you want to upload to
        "accessID": "", // Your access ID
        "credentialsJSONFile": "", // A JSON file containing your google cloud credentials
        "chunkSize": "262144", // Must be a multiple of 256K (262144 bytes)
        "uploadURLCacheFile": ".uploadURLCache.json", // A file for caching upload locations.
    }
}

func printResponse(resp *http.Response, doPrint bool) {
    if !doPrint {
        return
    }
    fmt.Println("response:" + resp.Status)
    for k, v := range resp.Header {
        fmt.Println("  header:", k, ":", v)
    }

    bodyBytes, err := ioutil.ReadAll(resp.Body)
    resp.Body.Close()
    if err != nil {
        log.Fatal(err)
    }
    body := string(bodyBytes[:])
    fmt.Println("  body:", body)
}

func printRequest(req *http.Request, verbose bool) {
    if !verbose {
        return
    }
    for k, v := range req.Header {
        fmt.Println("  req header:", k, ":", v)
    }
}

func extractUploadProgress(resp *http.Response) int64 {
    rangeString := resp.Header.Get("Range")
    split := strings.Split(rangeString, "-")
    if len(split) == 2 {
        rangeEnd, err := strconv.ParseInt(split[1], 10, 64)
        if err == nil {
            return rangeEnd
        }
    }
    return 0
}

func createPutRequest(location string, reader io.Reader, contentLength int64, rangeStart int64, rangeEnd int64, concludeUpload bool) *http.Request {
    req, err := http.NewRequest("PUT", location, reader)
    if err != nil {
        log.Fatal(err)
    }
    req.Header.Add("Content-Length", strconv.FormatInt(contentLength, 10))

    var rangeString string
    if contentLength == 0 {
        // We use this for querying the current upload progress
        rangeString = "*/*"
    } else {
        rangeString = strconv.FormatInt(rangeStart, 10) + "-" + strconv.FormatInt(rangeEnd, 10) + "/"
        // The final chunk must set the total file size.
        if concludeUpload {
            rangeString += strconv.FormatInt(rangeEnd + 1, 10)
        } else {
            rangeString += "*"
        }
    }
    req.Header.Add("Content-Range", "bytes " + rangeString)
    return req
}

func uploadFile(location string, fileName string, maxChunks int, verbose bool) {
    // Open the file for reading
    file, err := os.Open(fileName)
    if err != nil {
        log.Fatal(err)
    }

    // Determine the size of the file
    fileInfo, err := file.Stat()
    if err != nil {
        log.Fatal(err)
    }

    chunkSize, _ := strconv.ParseInt(config["chunkSize"], 10, 64)
    chunksInFile := int(fileInfo.Size() / chunkSize)
    if (int64(chunksInFile) * chunkSize) < fileInfo.Size() {
        chunksInFile = chunksInFile + 1
    }
    chunksUploading := chunksInFile
    if chunksUploading > maxChunks && maxChunks != -1 {
        chunksUploading = maxChunks
    }
    fmt.Printf("Uploading a total of %d chunks of %d bytes each.\n", chunksUploading, chunkSize)

    // Query current upload status
    query := createPutRequest(location, nil, 0, 0, 0, false)
    printRequest(query, verbose)
    client := &http.Client{}
    resp, err := client.Do(query)
    if err != nil {
        log.Fatal(err)
    }
    uploadProgress := extractUploadProgress(resp)
    bytesUploaded := uploadProgress
    if bytesUploaded > 0 {
        bytesUploaded = bytesUploaded + 1
    }
    printResponse(resp, verbose)
    fmt.Printf("There are already %d bytes uploaded.\n", bytesUploaded)

    // Reading and sending a single chunk at a time
    dataChunk := make([]byte, chunkSize)
    for i := int(bytesUploaded / chunkSize); i < chunksUploading; i++ {
        fmt.Printf("Uploading chunk %d\n", i)

        if _, err := file.Seek(uploadProgress, 0); err != nil {
            log.Fatal(err)
        }

        bytesRead, err := file.Read(dataChunk)
        if err != nil {
            log.Fatal(err)
        }
        
        reader := bytes.NewReader(dataChunk)
        // This will make the reader return EOF after 'bytesRead' number of bytes
        limitReader := io.LimitReader(reader, int64(bytesRead))
        
        // Create and HTTP Request, fill in the headers and the payload data
        rangeStart := int64(i) * chunkSize
        rangeEnd := rangeStart + int64(bytesRead) - 1
        req := createPutRequest(location, limitReader, int64(bytesRead), rangeStart, rangeEnd, i == (chunksInFile - 1))

        resp, err := client.Do(req)
        if err != nil {
            log.Fatal(err)
        }
        printResponse(resp, verbose)
    }
    file.Close()

}

func storeToJSONFile(key string, value string) {
    // Read existing file into memory and unmarshal the json data
    jsonData, _ := ioutil.ReadFile(config["uploadURLCacheFile"])
    var jsonMap map[string]string
    json.Unmarshal(jsonData, &jsonMap)
    if jsonMap == nil {
        jsonMap = make(map[string]string)
    }

    // Add the new value to the map, and write the map back to the file.
    jsonMap[key] = value
    jsonData, _ = json.Marshal(jsonMap)
    if err := ioutil.WriteFile(config["uploadURLCacheFile"], jsonData, 0644); err != nil {
        log.Fatal(err)
    }
}

func readFromJSONFile(key string) string {
    jsonData, _ := ioutil.ReadFile(config["uploadURLCacheFile"])
    var jsonMap map[string]string
    json.Unmarshal(jsonData, &jsonMap)
    return jsonMap[key]
}

func main() {
    // Parsing the command line
    filePathPtr := flag.String("file", "", "The path to a file")
    chunksToUploadPtr := flag.Int("chunks", -1, "Number of chunks to upload or -1 for the whole file.")
    verbosePtr := flag.Bool("verbose", false, "Print debug information")
    flag.Parse()

    if *filePathPtr == "" {
        flag.Usage()
        os.Exit(1)
    }

    // Initialize configuration and prioritze settings from .gcr-upload.conf
    config = staticConfiguration()
    localDir, err := filepath.Abs(filepath.Dir(os.Args[0]))
    if err != nil {
            log.Fatal(err)
    }
    jsonConfig, _ := ioutil.ReadFile(filepath.Join(localDir, ".gcr-upload.conf"))
    if jsonConfig != nil {
        var jsonMap map[string]string
        if err := json.Unmarshal(jsonConfig, &jsonMap); err != nil {
            log.Fatal(err)
        }
        // Copy configuration entries from jsonMap into our config.
        for key, value := range jsonMap {
            config[key] = value
        }
    }

    // The credentials file must be created in the google cloud developer console.
    if config["credentialsJSONFile"] == "" {
        log.Fatal("You have not defined a credentials file in your configuration.")
    }
    fmt.Println("Reading credentials from json keyfile: " + config["credentialsJSONFile"])
    data, err := ioutil.ReadFile(config["credentialsJSONFile"])
    if err != nil {
        log.Fatal(err)
    }

    authConf, err := google.JWTConfigFromJSON(data)
    if err != nil {
        log.Fatal(err)
    }

    filename := filepath.Base(*filePathPtr)
    location := readFromJSONFile(filename)
    // If we do not have an upload URL for this file yet, create a new one.
    if location != "" {
        fmt.Println("Found upload location in cache.")
    } else {
        fmt.Println("Creating signed url")
        method := "POST"
        // The signed URL will only be valid for 60 seconds.
        // Once we initiated the upload, we do not need the signed URL anymore.
        expires := time.Now().Add(time.Second * 60)
        signedURL, err := storage.SignedURL(config["bucket"], filename, &storage.SignedURLOptions{
            GoogleAccessID: config["accessID"],
            PrivateKey:     authConf.PrivateKey,
            Method:         method,
            Expires:        expires,
            Headers:        []string{"x-goog-resumable:start\n"},
        })
        if err != nil {
            log.Fatal(err)
        }
        if *verbosePtr {
            fmt.Println("signedUrl = " + signedURL)
        }

        // Initiating a resumable upload using the generated signed URL.
        fmt.Println("Initiating resumable upload")
        client := &http.Client{}
        req, err := http.NewRequest(method, signedURL, nil)
        if err != nil {
            log.Fatal(err)
        }
        req.Header.Add("Content-Length", "0")
        req.Header.Add("x-goog-resumable", "start")
        resp, err := client.Do(req)
        if err != nil {
            log.Fatal(err)
        }
        printResponse(resp, *verbosePtr)

        // Set the upload location received from the server.
        location = resp.Header["Location"][0]
        fmt.Println("Received new location from server.")
        storeToJSONFile(filename, location)
    }
    if *verbosePtr {
        fmt.Println("location: " + location)
    }

    uploadFile(location, *filePathPtr, *chunksToUploadPtr, *verbosePtr)
}

